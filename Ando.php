<?php 

class Omega extends Alpha{

public function __construct() {
         echo "<p>". __CLASS__ ." Class construct </p>";
    }

    function __destruct() {
         echo "<p>".__CLASS__ . " Class destruct </p>";
    }
    
}

class Alpha{

public function __construct() {
        echo "<p>". __CLASS__ ." Class construct </p>";
    }

    function __destruct() {
         echo "<p>".__CLASS__ . " Class destruct </p>";
    }
    
}